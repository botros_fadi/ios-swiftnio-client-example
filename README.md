# Simple SwiftNIO client demo

## What is it?

 - It is a very simple (not engineered, and non-reusable) example of how to use [SwiftNIO](https://github.com/apple/swift-nio)
  in an iOS application
  
## What's next?

 - Make this code reusable
 - Make it customizable (parameters, file upload, websocket support)
 - Make it follow redirect
 - Allow retry
 
## For more information and inspiration about networking on iOS:

[The modern `Network.framework` on iOS, and the older alternative (sockets)](https://developer.apple.com/videos/play/wwdc2018/715/)
