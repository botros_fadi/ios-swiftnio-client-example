//
//  NetworkingLayer.swift
//  SwiftNIOClientCocoapods
//
//  Created by fadi on 8/13/18.
//  Copyright © 2018 fadi. All rights reserved.
//

import UIKit
import NIO
import NIOHTTP1
import NIOWebSocket


/// A connection read/write handler
// TODO: Make it customizable using closures to make callbacks
class Handler: ChannelInboundHandler {
    typealias InboundIn = HTTPClientResponsePart
    
    func channelRead(ctx: ChannelHandlerContext, data: NIOAny) {
        let data = self.unwrapInboundIn(data)
        switch data {
        case .head(let h):
            print("Headers \(h)")
        case .body(let b):
            var c = b
            print("ByteBuffer: \(c.readString(length: c.readableBytes) ?? "<NIL>")")
        case .end(let h):
            print("Ending \(h?.description ?? "<NIL>")")
            ctx.close(mode: .all, promise: nil)
        }
    }
    
    
}

class NetworkingLayer {
    // Prepare the connector
    let bootstrap = ClientBootstrap(group: MultiThreadedEventLoopGroup.init(numberOfThreads: 1)).channelInitializer({channel in
        let _ = channel.pipeline.addHTTPClientHandlers()
        return channel.pipeline.add(handler: Handler())
    })
        .channelOption(ChannelOptions.socket(SocketOptionLevel(SOL_SOCKET), SO_REUSEADDR), value: 1)
        .channelOption(ChannelOptions.socket(IPPROTO_TCP, TCP_NODELAY), value: 1)

    // TODO: Make it customizable, parameters, headers, etc...
    func makeARequest(host: String, path: String) -> EventLoopFuture<Void>? {
        let channel = try? bootstrap.connect(to: .newAddressResolving(host: host, port: 80))
        return channel?.then({ (connectedChannel) in
            var h = HTTPRequestHead.init(version: .init(major: 1, minor: 1), method: .GET, uri: path)
            h.headers.add(name: "Host", value: host)
            h.headers.add(name: "User-Agent", value: "Application/5.0 (Macintosh; Intel Mac OS X 10.13; rv:61.0)")
            h.headers.add(name: "Accept", value: "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
            h.headers.add(name: "Accept-Language", value: "en-US,en;q=0.5")
            h.headers.add(name: "Connection", value: "keep-alive")
            h.headers.add(name: "Upgrade-Insecure-Requests", value: "1")
            let _ = connectedChannel.write(NIOAny.init(HTTPClientRequestPart.head(h)))
            return connectedChannel.writeAndFlush(NIOAny.init(HTTPClientRequestPart.end(nil))).then({ _ in
                return connectedChannel.closeFuture
            })
        })
    }
    
    func makeExampleTwoConcurrentRequests() -> EventLoopFuture<Void>? {
        return makeARequest(host: "web.stanford.edu", path: "/class/cs193p/cgi-bin/drupal/")?.and(makeARequest(host: "www.pearsoned.co.uk", path: "/bookshop/detail.asp?item=100000000555425")!).map({_ in return})
    }
}
